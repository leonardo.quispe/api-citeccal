package com.citeccal.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="fichas")
@Getter @Setter
public class FichaTecnica {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String linea;
	
	private String serie;
	
	private Integer alTaco;
	
	private String color;
	
	private String estilo;
	
	private String codHorma;
	
	private String codPlanta;
	
	private String c1Tipo;
	
	private String c1Color;
	
	private String c2Tipo;
	
	private String c2Color;
	
	private String c3Tipo;
	
	private String c3Color;
	
	private String f1Tipo;
	
	private String f1Color;
	
	private String f2Tipo;
	
	private String f2Color;
	
	private String h1Tipo;
	
	private Integer h1Numero;
	
	private String h1Color;
	
	private String h2Tipo;
	
	private Integer h2Numero;
	
	private String h2Color;
	
	private String h3Tipo;
	
	private Integer h3Numero;
	
	private String h3Color;

	private String acc1Detalle;

	private String acc1Material;

	private String acc1Color;

	private String acc2Detalle;

	private String acc2Material;

	private String acc2Color;

	private String acc3Detalle;

	private String acc3Material;

	private String acc3Color;

	private String cierreDetalle;

	private String cierreMaterial;

	private String cierreColor;

	private String pullerDetalle;

	private String pullerMaterial;

	private String pullerColor;

	private String punteraMaterial;

	private String contraMaterial;

	private String p1Material;
	
	private String p1Color;

	private String p1Forrado;

	private String p2Material;

	private String p2Color;

	private String p2Forrado;

	private String plataformaMaterial;

	private String plataformaColor;

	private String plataformaForrado;

	private String tacoMaterial;

	private String tacoColor;
	
	private String tacoForrado;

	private String plantillaMaterial;

	private String plantillaColor;

	private String acolcheMaterial;

	private String h4Tipo;

	private Integer h4Numero;

	private String h4Color;

	private String costuraTipo;
	
	private String selloMarca;
	
	private String selloTipo;
	
	private String selloMaterial;

	private String cueritoMarca;

	private String cueritoTipo;

	private String cueritoMaterial;
	
	private String hantagMarca;
	
    private String imgRuta1;
	
    private String imgRuta2;
	
    private String imgRuta3;
	
    private String imgRuta4;
	
}
