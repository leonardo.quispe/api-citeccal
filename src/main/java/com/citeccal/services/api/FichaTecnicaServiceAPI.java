package com.citeccal.services.api;

import com.citeccal.models.FichaTecnica;
import com.citeccal.commons.GenericServiceAPI;

public interface FichaTecnicaServiceAPI extends GenericServiceAPI<FichaTecnica, Long>  {

}