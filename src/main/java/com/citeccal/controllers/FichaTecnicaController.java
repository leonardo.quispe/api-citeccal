package com.citeccal.controllers;

/*import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;*/
import java.util.List;

/*import org.apache.tomcat.jni.File;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
/*import org.springframework.web.bind.annotation.RequestParam;*/
import org.springframework.web.bind.annotation.RestController;
/*import org.springframework.web.multipart.MultipartFile;*/

import com.citeccal.models.FichaTecnica;
import com.citeccal.services.api.FichaTecnicaServiceAPI;

@RestController
@RequestMapping(value = "/api/ficha/")
public class FichaTecnicaController {
	
	@Autowired
	private FichaTecnicaServiceAPI fichaTecnicaServiceAPI;
	
	@GetMapping(value = "/all")
	public List<FichaTecnica> getAll() {
		return fichaTecnicaServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public FichaTecnica find(@PathVariable Long id) {
		return fichaTecnicaServiceAPI.get(id);
	}
	/*public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/imagedata";*/
	
	@PostMapping(value = "/save")
	public ResponseEntity<FichaTecnica> save(@RequestBody FichaTecnica fichaTecnica) {
		FichaTecnica obj = fichaTecnicaServiceAPI.save(fichaTecnica);
		return new ResponseEntity<FichaTecnica>(obj, HttpStatus.OK);
	}
	/*@PostMapping(value = "/save")
	public ResponseEntity<FichaTecnica> save(@RequestBody FichaTecnica fichaTecnica, @RequestParam("img") MultipartFile file ) {
		
		StringBuilder fileNames = new StringBuilder();
		String filename = fichaTecnica.getId() + file.getOriginalFilename().substring(file.getOriginalFilename().length()-4);
		String url =  "http://192.168.1.12:8080" + filename;
		Path fileNameAndPath =Paths.get(uploadDirectory,url);
		
		try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fichaTecnica.setImgRuta1(url);
		fichaTecnica.setImgRuta2(filename);
		fichaTecnica.setImgRuta3(filename);
		fichaTecnica.setImgRuta4(filename);
		
		FichaTecnica obj = fichaTecnicaServiceAPI.save(fichaTecnica);
		return new ResponseEntity<FichaTecnica>(obj, HttpStatus.OK);
	}*/
	

	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<FichaTecnica> delete(@PathVariable Long id) {
		FichaTecnica fichaTecnica = fichaTecnicaServiceAPI.get(id);
		if (fichaTecnica != null) {
			fichaTecnicaServiceAPI.delete(id);
		} else {
			return new ResponseEntity<FichaTecnica>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<FichaTecnica>(fichaTecnica, HttpStatus.OK);
	}

}
